﻿using System.Net;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;

namespace webapi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseKestrel((context, options) =>
                {
                    // Set properties and call methods on options
                    options.Listen(IPAddress.Loopback, 8080);
                    options.Listen(IPAddress.Loopback, 5000);
                })
                .UseStartup<Startup>();
    }
}
