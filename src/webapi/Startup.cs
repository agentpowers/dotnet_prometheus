﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Prometheus;

namespace webapi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // restrict /metrics to be accessed only via port 50000
            app.Use(async (context, next) => 
            {
                var isMetricsEndpoint = string.Equals(context.Request.Path, "/metrics", StringComparison.OrdinalIgnoreCase);
                var isMetricsPort = context.Request.Host.Port == 5000;
                
                if ((isMetricsPort && !isMetricsEndpoint)
                    || (!isMetricsPort && isMetricsEndpoint))
                {
                    context.Response.StatusCode = 404; //Method Not Allowed               
                    return;
                }
                await next.Invoke(); 
            });

            app.Map("/metrics", metricsApp =>
            {
                // We already specified URL prefix in .Map() above, no need to specify it again here.
                metricsApp.UseMetricServer("");
                metricsApp.UseHttpMetrics();
            });
            app.UseMvc();
        }
    }
}
